# Archlinux Fresh LXDM theme
![Archlinux-fresh image](https://gitlab.com/jorisvandijk/archlinux-fresh/-/raw/master/PREVIEW.png "Archlinux-fresh")

This is an LXDM theme, based on the original found in the AUR.
It has been altered to have a *stylish* background and the "User:" & "Passord:" texts
have been removed, as well as the clock. This to give the overal look a more pleasing visual style.

To install:
```
git clone https://gitlab.com/jorisvandijk/archlinux-fresh.git /usr/share/lxdm/themes/Archlinux-fresh
rm /etc/lxdm/lxdm.conf
mv /usr/share/lxdm/themes/Archlinux-fresh/lxdm.conf /etc/lxdm/lxdm.conf
mv /usr/share/lxdm/themes/Archlinux-fresh/.dmrc $HOME/.dmrc
```
Or run the install script as root.

This LXDM theme only works with LXDM, not with LXDM-gtk3.
